import java.util.ArrayList;

/**
 *
 * @author 
 */
public class ExhaustiveSearchTest {
    public static void main(String[] args) {
        int[] a = {1, 3, 5, 7, 10, 4, 8};
        showInput(a);
        ExhaustiveSearch exs = new ExhaustiveSearch(a);
        exs.process();
        //show all pair subset
//        exs.getDisjointArray1();
//        System.out.println("------------------------");
//        exs.getDisjointArray2();
        exs.sum(); //sum and show result

    }

    private static void showInput(int[] a) {
        System.out.print("Input is: ");
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        System.out.println("");
    }
    }


